package com.zycus.crms.bus.bo;

import java.io.Serializable;

public class GdsReportData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8475993862297112444L;
	
	private String tenantId;
	private String product;
	private String entityName;
	private String recievedDate;
	private Long receivedCount;
	private Long processedCount;
	private Long failedParkedCount;
	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}
	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}
	/**
	 * @return the recievedDate
	 */
	public String getRecievedDate() {
		return recievedDate;
	}
	/**
	 * @param recievedDate the recievedDate to set
	 */
	public void setRecievedDate(String recievedDate) {
		this.recievedDate = recievedDate;
	}
	/**
	 * @return the receivedCount
	 */
	public Long getReceivedCount()
	{
		return receivedCount;
	}
	/**
	 * @param receivedCount the receivedCount to set
	 */
	public void setReceivedCount(Long receivedCount)
	{
		this.receivedCount = receivedCount;
	}
	/**
	 * @return the processedCount
	 */
	public Long getProcessedCount()
	{
		return processedCount;
	}
	/**
	 * @param processedCount the processedCount to set
	 */
	public void setProcessedCount(Long processedCount)
	{
		this.processedCount = processedCount;
	}
	/**
	 * @return the failedParkedCount
	 */
	public Long getFailedParkedCount()
	{
		return failedParkedCount;
	}
	/**
	 * @param failedParkedCount the failedParkedCount to set
	 */
	public void setFailedParkedCount(Long failedParkedCount)
	{
		this.failedParkedCount = failedParkedCount;
	}
	/**
	 * @return the entityName
	 */
	public String getEntityName()
	{
		return entityName;
	}
	/**
	 * @param entityName the entityName to set
	 */
	public void setEntityName(String entityName)
	{
		this.entityName = entityName;
	}
}
