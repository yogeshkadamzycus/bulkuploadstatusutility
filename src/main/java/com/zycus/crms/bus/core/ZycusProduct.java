package com.zycus.crms.bus.core;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author onkar.deshpande
 *
 */
public enum ZycusProduct
{
	/*** iPerform(SPM) */
	SPM("SPM"),

	/*** iAnalyze */
	iAnalyze("iAnalyze"),

	/*** iContract */
	iContract("iContract"),

	/*** iSource */
	iSource("iSource"),

	/*** iSave */
	iSave("iSave"),

	/*** iSave */
	iMine("iMine"),

	/*** iSave */
	iCost("iCost"),

	/*** iSupplier(SIM) */
	SIM("SIM"),

	/*** P2P(eProc) */
	eProc("eProc"),

	/*** OneView */
	OneView("OneView"),

	/** iMaster */
	iMaster("CMD"),

	/** iManage */
	iManage("iManage"),

	/** iRequest */
	iRequest("iRequest"),

	/** eInvoice */
	eInvoice("eInvoice"),

	/** Rainbow */
	Rainbow("Rainbow"),

	/** TMS */
	TMS("TMS"),

	/** Dashboard */
	Dashboard("Dashboard"),

	/** Field Library */
	FieldLibrary("FieldLibrary"),

	/** ZCS */
	ZCS("ZCS"),

	/** ReportStudio */
	CentralRMS("ReportStudio"),

	/**
	 *Centalize Global Delievery System
	 */
	CGDS("cgds"),

	/**
	 * 
	 */
	LMT("lmt"),

	/**
	 * 
	 */
	CWF("CWF"),
	
	/** Unknown */
	Unknown("Unknown");

	private final static Map<String, ZycusProduct>	productAlternateNames	= new HashMap<>();

	private String									name;

	static
	{
		/* Used to identify product enum with alternate names */
		for (ZycusProduct product : ZycusProduct.values())
		{
			productAlternateNames.put(product.name, product);
		}
	}

	/** private constructor, for alternate name */
	private ZycusProduct(String name)
	{
		this.name = name;
	}

	public char[] shortCode()
	{
		return null;
	}

	/**
	 * Returns the name of the product as a String.
	 * 
	 * @return {@link String}
	 */
	public String getProductName()
	{
		return name;
	}

	public static ZycusProduct fromNameIgnoreCase(String name)
	{

		for (String productName : productAlternateNames.keySet())
		{
			if (productName.equalsIgnoreCase(name))
				return productAlternateNames.get(productName);
		}

		return ZycusProduct.Unknown;
	}
}
