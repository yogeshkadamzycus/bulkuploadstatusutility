package com.zycus.crms.bus.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author onkar.deshpande
 *
 */
public class StatusUtility
{
	/**
	 0 	: 	"tenantId",
	 1	:	"product",
	 2	:	"entityName",
	 3	:	Metadata Version [metadataVersion] 
	 4	:	"Total Count" [lastRecordCount] 
	 5	:	"Remaining (Count Difference)" [countDiff]
	 6	:	Export Started On [exportStartedOn]
	 7	:	Export Completed On [exportCompletedOn]
	 8	:	Import Started On[importStartedOn]
	 9	:	Import Completed On[importCompletedOn]
	 *
	 */
	private static String[]		columns		= { "Company Name", "Company(tenant) ID", "Product", "Entity Name",
			"Metadata Version", "Total Count", "Remaining (Count Difference)", "Export Started On",
			"Export Completed On", "Import Started On", "Import Completed On" };

	public static final String	ALL			= "all";

	private static final long	SLEEP_TIME	= 10000;
	private static final long	TRY_COUNT	= 6;

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception
	{
		String s = "##############################################################################################";
		System.out.println("");
		System.out.println(s);
		System.out.println("");
		s = "[" + new Date() + "] Bulk Upload Status Utility started ...";
		System.out.println(s);
		Properties prop = new Properties();
		try
		{
			String currentDir = new File(".").getCanonicalPath();
			prop.load(new FileInputStream(currentDir + File.separator + "config.properties"));
		}
		catch (IOException e)
		{
			s = "[" + new Date() + "] Unable to read input file input.txt";
			System.out.println(s);
		}
		List<String> tenants = new ArrayList<String>();
		String baseUrl = (String) prop.get("base_url");
		String tenantsString = (String) prop.get("tenant_list");
		if (ALL.equalsIgnoreCase(tenantsString))
			tenants = getTenantListFromServer(baseUrl);
		else
		{
			String[] tenantArr = tenantsString.split(",");
			tenants = Arrays.asList(tenantArr);
		}
		performJob(tenants, baseUrl);
		getGDSReport(baseUrl);
		s = "[" + new Date() + "] Bulk Upload Status Utility completed";
		System.out.println(s);
	}

	private static void getGDSReport(String baseUrl) throws Exception {
		String url = baseUrl + "/service/utility/gdsReportData";
		String gdsResponseData = HTTPClientUtil.get(null, url);
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		Instant instant = Instant.now();
		String jsonFileName = s + File.separator + "GDS_" + instant.getEpochSecond() + ".json";
		Files.write(Paths.get(jsonFileName), gdsResponseData.getBytes());		
	}

	private static void performJob(List<String> tenants, String baseUrl)
			throws Exception, JSONException, IOException, ParseException
	{
		List<TenantData> tenantsData = new ArrayList<>();
		for (String tenant : tenants)
		{
			JSONArray resposeData = null;
			System.out.println("[" + new Date() + "] Process started for tenant ID : " + tenant);
			Map<String, Set<String>> resMap = getWhitelistedProduct(baseUrl, tenant);
			if (resMap.isEmpty())
			{
				continue;
			}
			Entry<String, Set<String>> entry = resMap.entrySet().iterator().next();
			String companyName = entry.getKey();
			Set<String> whitelistedProduct = entry.getValue();
			Integer count = 1;
			do
			{
				System.out.println("[" + new Date() + "] Fetching tenant data from the product. Try count : " + count);
				resposeData = processForTenant(baseUrl, tenant);
				Set<String> productsReceived = getProductsReceived(resposeData);
				Set<String> remaingProducts = difference(whitelistedProduct, productsReceived);
				if (productsReceived.containsAll(whitelistedProduct))
				{
					System.out.println("[" + new Date() + "] Got data for all required products...");
					break;
				}
				else if (count == TRY_COUNT)
				{
					System.out.println("[" + new Date() + "] Tried " + TRY_COUNT
							+ " times to get tenant data for all products. Not received data for product(s) : "
							+ remaingProducts + ". Processing with remaining products...");
				}
				else
				{
					System.out.println("[" + new Date() + "] Data not received for product(s) : " + remaingProducts
							+ ". Retrying...");
				}
				count++;
				Thread.sleep(SLEEP_TIME);
			}
			while (count <= TRY_COUNT);
			List<TenantData> tenantData = convertResponseToPrintList(companyName, resposeData);
			tenantsData.addAll(tenantData);
			System.out.println("[" + new Date() + "] Process completed for tenant ID " + tenant);
			System.out.println();
			System.out.println(
					"-------------------------------------------------------------------------------------------------------------");
			System.out.println();
		}

		System.out.println("[" + new Date() + "] Exporting data to file ...");
		writeDataToFile(tenantsData);
	}

	/**
	 * @param setOne
	 * @param setTwo
	 * @return
	 */
	public static <T> Set<T> difference(final Set<T> setOne, final Set<T> setTwo)
	{
		Set<T> result = new HashSet<T>(setOne);
		result.removeAll(setTwo);
		return result;
	}

	private static Set<String> getProductsReceived(JSONArray resposeData) throws JSONException
	{
		Set<String> receivedProducts = new HashSet<>();
		for (int i = 0; i < resposeData.length(); i++)
		{
			JSONObject obj = resposeData.getJSONObject(i);
			JSONObject key = (JSONObject) obj.get("key");
			String product = (String) key.get("product");
			receivedProducts.add(product);
		}
		return receivedProducts;
	}

	private static JSONArray processForTenant(String baseUrl, String tenant) throws Exception
	{

		JSONArray responseData = getDataForTenant(tenant, baseUrl);
		if (responseData == null || !(responseData.length() > 0))
		{
			System.out.println(
					"[" + new Date() + "] Not get response from server. Retrying for tenant ID " + tenant + " ...");
			Thread.sleep(SLEEP_TIME);
			processForTenant(baseUrl, tenant);
		}

		return responseData;
	}

	@SuppressWarnings("unchecked")
	private static List<String> getTenantListFromServer(String baseUrl) throws Exception
	{
		List<String> list = new ArrayList<>();
		String s = "[" + new Date() + "] Retriving all tenant id(s) from server for status data...";
		System.out.println(s);
		String url = baseUrl + "/service/utility/tenantList";
		JSONObject jsonObject = new JSONObject(HTTPClientUtil.get(null, url));
		JSONArray array = (JSONArray) jsonObject.get("result");
		for (int i = 0; i < array.length(); i++)
		{
			list.add(array.getString(i));
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	private static Map<String, Set<String>> getWhitelistedProduct(String baseUrl, String tenantId) throws Exception
	{
		Map<String, Set<String>> resultMap = new HashMap<>();
		Set<String> products = new HashSet<>();
		String s = "[" + new Date() + "] Retriving white-listed products for tenant " + tenantId;
		System.out.println(s);
		String url = baseUrl + "/service/utility/tenantInternalProductUrl?tenantId=" + tenantId;
		JSONObject jsonObject = new JSONObject(HTTPClientUtil.get(null, url));
		Object result = jsonObject.get("result");
		if (JSONObject.NULL.equals(result))
		{
			System.out.println("[" + new Date() + "] Unable to get white-listed products for tenant " + tenantId);
			return resultMap;
		}
		try 
		{
		    	JSONObject resObj = (JSONObject) result;
			String companyName = resObj.getString("companyName");
			JSONObject prod = (JSONObject) resObj.get("productMap");
			Map<ZycusProduct, String> productsMap = new Gson().fromJson(prod.toString(),
					new TypeToken<HashMap<ZycusProduct, String>>()
					{}.getType());

			Set<ZycusProduct> keys = productsMap.keySet();
			for (ZycusProduct product : keys)
			{
			    try {
				
				if (productsMap.get(product) != null)
					products.add(product.toString());
			    }
			    catch(Exception e)
			    {
				    System.out.println(e);
				    System.out.println("Exception while get the product from the map for  product "+product);
				    
			    }
				
			}
			resultMap.put(companyName, products);
		}
		catch(Exception e)
		{
		    System.out.println(e);
		    System.out.println("Exception while converting result to map"+((JSONObject)jsonObject.get("result")).toString());
		   
		}
		
		return resultMap;
	}

	private static void writeDataToFile(List<TenantData> list) throws IOException, ParseException
	{
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		LocalDateTime now = LocalDateTime.now();
		String fileName = s + File.separator + "BulkUploadStatus_"
				+ now.toString().replaceAll("-", "_").replace(":", "_") + ".xlsx";
		File file = new File(fileName);
		if (!file.createNewFile())
		{
			s = "[" + new Date() + "] Unable to create excel file " + fileName + "!!!";
			System.out.println(s);
		}
		
		Date jsonTime = Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
		String jsonFileName = s + File.separator + "BulkUploadStatus_"+ jsonTime.getTime()+ ".json";
		
		File jsonFile = new File(jsonFileName);
		if (!jsonFile.createNewFile())
		{
			s = "[" + new Date() + "] Unable to create text file " + jsonFileName + "!!!";
			System.out.println(s);
		}

		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

		CreationHelper createHelper = workbook.getCreationHelper();

		Sheet sheet = workbook.createSheet("BulkUploadStatus");

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.BLUE.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++)
		{
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy hh:mm:ss"));
		Calendar calendar = Calendar.getInstance();
		int invalidYear = 1970;
		Date date = null;
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		// Create Other rows and cells with employees data
		int rowNum = 1;
		List<TenantData> printList = new ArrayList<>();
		for (TenantData data : list)
		{
		    	TenantData printData = data;
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(data.companyName);
			row.createCell(1).setCellValue(data.tenantId);

			row.createCell(2).setCellValue(data.product);
			row.createCell(3).setCellValue(data.entityName);
			row.createCell(4).setCellValue(data.metadataVersion);
			row.createCell(5).setCellValue(data.lastRecordCount);

			row.createCell(6).setCellValue(data.countDiff);
			
			Cell rowCell7 = row.createCell(7);
			if (data.exportStartedOn != null)
			{
				date = format.parse(data.exportStartedOn);
				calendar.setTime(date);
				int year = calendar.get(Calendar.YEAR);

				if (year == invalidYear)
				{
					rowCell7.setCellValue("NOT STARTED");
					printData.exportStartedOn = "NOT STARTED";
				}
				else
				{
					rowCell7.setCellValue(date);
					printData.exportStartedOn = String.valueOf(date.getTime());
					rowCell7.setCellStyle(dateCellStyle);
				}
			}
			else
			{
				rowCell7.setCellValue("NOT STARTED");
				printData.exportStartedOn = "NOT STARTED";
			}

			Cell rowCell8 = row.createCell(8);
			if (data.exportCompletedOn != null)
			{
				date = format.parse(data.exportCompletedOn);
				calendar.setTime(date);
				int year = calendar.get(Calendar.YEAR);

				if (year == invalidYear)
				{
					rowCell8.setCellValue("NOT STARTED");
					printData.exportCompletedOn = "NOT STARTED";
				}
				else
				{
					rowCell8.setCellValue(date);
					printData.exportCompletedOn = String.valueOf(date.getTime());
					rowCell8.setCellStyle(dateCellStyle);
				}
			}
			else
			{
				rowCell8.setCellValue("NOT STARTED");
				printData.exportCompletedOn = "NOT STARTED";
			}

			Cell rowCell9 = row.createCell(9);
			if (data.importStartedOn != null)
			{
				date = format.parse(data.importStartedOn);
				calendar.setTime(date);
				int year = calendar.get(Calendar.YEAR);

				if (year == invalidYear)
				{
					rowCell9.setCellValue("NOT STARTED");
					printData.importStartedOn = "NOT STARTED";
				}
				else
				{
					rowCell9.setCellValue(date);
					printData.importStartedOn = String.valueOf(date.getTime());
					rowCell9.setCellStyle(dateCellStyle);
				}
			}
			else
			{
				rowCell9.setCellValue("NOT STARTED");
				printData.importStartedOn = "NOT STARTED";
			}

			Cell rowCell10 = row.createCell(10);
			if (data.importCompletedOn != null)
			{
				date = format.parse(data.importCompletedOn);
				calendar.setTime(date);
				int year = calendar.get(Calendar.YEAR);

				if (year == invalidYear)
				{
					rowCell10.setCellValue("NOT STARTED");
					printData.importCompletedOn = "NOT STARTED";
				}
				else
				{
					rowCell10.setCellValue(date);
					printData.importCompletedOn = String.valueOf(date.getTime());
					rowCell10.setCellStyle(dateCellStyle);
				}
			}
			else
			{
				rowCell10.setCellValue("NOT STARTED");
				printData.importCompletedOn = "NOT STARTED";
			}
			
			printList.add(printData);

		}

		for (int i = 0; i < columns.length; i++)
		{
			sheet.autoSizeColumn(i);
		}

		FileOutputStream fileOut = new FileOutputStream(fileName);
		String json = new Gson().toJson(printList);
		try(FileWriter writer = new FileWriter(jsonFile)) {
		    writer.write(json); 
		}
		catch(IOException e){
		    System.out.println("Exception while writing the json file"+e);
		}
		workbook.write(fileOut);
		fileOut.close();

		workbook.close();

	}

	private static List<TenantData> convertResponseToPrintList(String companyName, JSONArray tenantArray)
			throws JSONException
	{
		List<TenantData> list = new ArrayList<>();
		for (int i = 0; i < tenantArray.length(); i++)
		{
			TenantData data = new TenantData();
			JSONObject obj = tenantArray.getJSONObject(i);
			JSONObject key = (JSONObject) obj.get("key");
			JSONObject val = (JSONObject) obj.get("val");
			data.companyName = companyName;
			data.product = (String) key.get("product");
			data.entityName = (String) key.get("entityName");
			data.tenantId = (String) key.get("tenantId");
			long metaVersion = 0;
			Object versionObj = val.get("metadataVersion");
			if (versionObj instanceof Integer)
				metaVersion = ((Integer) versionObj).longValue();
			else if (versionObj instanceof Long)
				metaVersion = (long) versionObj;
			else if (versionObj instanceof String)
				metaVersion = Long.valueOf((String) versionObj);
			data.metadataVersion = metaVersion;
			data.lastRecordCount = (int) val.get("lastRecordCount");
			data.countDiff = (int) val.get("countDiff");
			if (!JSONObject.NULL.equals(val.get("exportStartedOn")))
				data.exportStartedOn = (String) val.get("exportStartedOn");
			if (!JSONObject.NULL.equals(val.get("exportCompletedOn")))
				data.exportCompletedOn = (String) val.get("exportCompletedOn");
			if (!JSONObject.NULL.equals(val.get("importStartedOn")))
				data.importStartedOn = (String) val.get("importStartedOn");
			if (!JSONObject.NULL.equals(val.get("importCompletedOn")))
				data.importCompletedOn = (String) val.get("importCompletedOn");
			list.add(data);
		}
		return list;
	}

	private static JSONArray getDataForTenant(String tenantId, String baseUrl) throws Exception
	{
		String url = baseUrl
				+ "/bulkUpload?action=status&shouldUpdateMetadata=false&shouldUpdateEntityCount=true&shouldClearAutoScheduled=false&tenant="
				+ tenantId;

		JSONObject responseData = new JSONObject(HTTPClientUtil.get(tenantId, url));
		return new JSONArray(responseData.get("payload").toString());
	}

	static class TenantData
	{
		String	companyName;
		String	tenantId;
		String	product;
		String	entityName;
		long	metadataVersion;
		int		lastRecordCount;
		int		countDiff;
		String	exportStartedOn;
		String	exportCompletedOn;
		String	importStartedOn;
		String	importCompletedOn;

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return "TenantData [tenantId=" + tenantId + ", product=" + product + ", entityName=" + entityName
					+ ", metadataVersion=" + metadataVersion + ", lastRecordCount=" + lastRecordCount + ", countDiff="
					+ countDiff + ", exportStartedOn=" + exportStartedOn + ", exportCompletedOn=" + exportCompletedOn
					+ "]";
		}

	}
}
