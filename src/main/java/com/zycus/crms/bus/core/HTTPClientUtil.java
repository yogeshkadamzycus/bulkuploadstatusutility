package com.zycus.crms.bus.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

public class HTTPClientUtil
{
	/**
	 * @param tenantId
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static String get(String tenantId, String url) throws Exception
	{
		HttpClient client = HttpClientBuilder.create().build();

		HttpGet request = new HttpGet(url);

		// add request header

		HttpResponse response;
		try
		{
			response = client.execute(request);
		}
		catch (Exception e)
		{
			throw new Exception("Unable to connect CRMS Server : " + e.getMessage(), e);
		}

		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		try
		{
			while ((line = rd.readLine()) != null)
			{
				result.append(line);
			}
		}
		catch (IOException e)
		{
			throw new Exception("Unable to read response from CRMS Server : " + e.getMessage(), e);
		}
		return result.toString();
	}
}
